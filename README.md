<img src="img/hsw-wide.jpg" width="250px">
# HSWBeamer
Eine Modifikation der LaTeX-Beamer-Klasse.<br />
Entwickelt für die Hochschule Wismar -- University of Applied Sciences: Technology, Business and Design

## Autor(en):
* Jonitz, Thomas [t.jonitz@wings.hs-wismar.de](mailto:t.jonitz@wings.hs-wismar.de)

## Quellen herunterladen
### Clone repository:
```
$ git clone https://xld@bitbucket.org/xld/hswbeamer.git <destination>
```
### Clone Wiki
```
$ git clone https://xld@bitbucket.org/xld/hswbeamer.git/wiki <destination>
```

## Verwendung
Das `img`-Verzeichnis, sowie die `hswbeamer.cls` müssen sich im selben Verzeichnis, wie die Haupt-Tex-Datei befinden.

Die Klasse wird wie folgt eingebunden:
```LaTeX
\documentclass[options]{hswbeamer}
```

### Style-Optionen
Nachfolgende Style-Optionen sind möglich (status der Implementierung)
- [x] **`adm`:** Farbschema Administration/Hochschulverwaltung
- [x] **`fww`:** Farbschema Fakultät für Wirtschaftswissenschaften
- [x] **`fiw`:** Farbschema Fakultät für Ingenieurswissenschaften und Seefahrt
- [x] **`fg`:** Farbschema für Fakultät Gestaltung
- [x] **`wings`:** Farbschema Wismar International Graduation Services GmbH
- [ ] **`clean`:** Weißer Hintergrund und Titel mit oben definiertem Farbschema; schwarzer Text  (@TODO)
- [ ] **`dark`:** Dunkelgrauer Hintergrund und Titel mit oben definiertem Farbschema; weißer Text (@TODO)
- [ ] **`silver`:** Dunkelgrauer Hintergrund und Titel mit oben definiertem Farbschema; variierender Text (@TODO)
Die Farbdefinitionen entsprechen dem Corporate Design der Hochschule Wismar

