%
%	Extended Beamer
%   LaTeX-Class for Presentations 
%   (e.g. at HSW)
%
%   author(s):  xld (info@xladde.de)
%
%   license(s):
%   The Source code of this document is provided under the terms of the
%               GNU GENERAL PUBLIC LICENSE v3
%               http://www.gnu.org/licenses/gpl-3.0.html    
%   The Theme options, color combinations and the logos are distributed under the
%   copyright of Wismar International Graduation Services GmbH and Hochschule Wismar
%
\LoadClass[aspectratio=169]{beamer} %1610, 149, 54, 32 und 43 for 16:10, 14:9, 5:4, 3:2 und 4:3.
%\LoadClass{beamer}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{hswbeamer}[2017/02/10 Custom class for Presentations]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   VARIABLES
%
\newcommand{\INSTITUTECOLORCODE}{0,0,0}
\newcommand{\INSTITUTE}{Hochschule Wismar -- University of applied sciences: technology, business and design}
\newcommand{\INSTITUTESHORT}{HSW}
\newcommand{\INSTITUTELOGO}{img/fischer-black.png}

\newcommand{\BGCOLOR}{white}
\newcommand{\BGCOLORCODE}{255,255,255}
\newcommand{\TEXTCOLOR}{black}
\newcommand{\TEXTCOLORCODE}{0,0,0}
\newcommand{\INSTITUTECOLOR}{adm}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   DEPARTMENT AND THEME SPECIFIC CONFIGURATIONS
%
%   The following Options can be configured:
%   1. Departments of HSW (adm, fww, fiw, fg, wings)
%   2. Theme options (dark, silver, clean)

%
% Configures colors and Logo for administration departments (dark grey, or silver)
%
%
%
\DeclareOption{adm}{\relax}

%
% Configures colors and Logo for department of economics (green)
%
\DeclareOption{fww}{
    \renewcommand{\INSTITUTECOLORCODE}{51,153,51}
    \renewcommand{\INSTITUTE}{Hochschule Wismar -- Fakult\"{a}t f\"{u}r Wirtschaftswissenschaften}
    \renewcommand{\INSTITUTESHORT}{FWW}
    \renewcommand{\INSTITUTELOGO}{img/fischer-fww.png}
    \renewcommand{\INSTITUTECOLOR}{fww}
}

%
% Configures colors and Logo for adepartment of engineering (blue)
%
\DeclareOption{fiw}{
    \renewcommand{\INSTITUTECOLORCODE}{0,177,219}
    \renewcommand{\INSTITUTE}{Hochschule Wismar -- Fakult\"{a}t f\"{u}r Ingenieurswissenschaften}
    \renewcommand{\INSTITUTESHORT}{FIW}
    \renewcommand{\INSTITUTELOGO}{img/fischer-fiw.png}
    \renewcommand{\INSTITUTECOLOR}{fiw}
}

%
% Configures colors and Logo for department of design (orange)
%
\DeclareOption{fg}{
    \renewcommand{\INSTITUTECOLORCODE}{255,93,2}
    \renewcommand{\INSTITUTE}{Hochschule Wismar -- Fakult\"{a}t Gestaltung}
    \renewcommand{\INSTITUTESHORT}{FG}
    \renewcommand{\INSTITUTELOGO}{img/fischer-fg.png}
    \renewcommand{\INSTITUTECOLOR}{fg}
}

%
% Configures colors and Logo for WINGS gmbh
%
\DeclareOption{wings}{
    \renewcommand{\INSTITUTECOLORCODE}{0,50,95}
    \renewcommand{\INSTITUTE}{Wismar International Graduation Services GmbH}
    \renewcommand{\INSTITUTESHORT}{WINGS GmbH}
    \renewcommand{\INSTITUTELOGO}{img/fischer-wings.png}
    \renewcommand{\INSTITUTECOLOR}{wings}
}


%
% Configures colors and Logo for administration departments
%
\DeclareOption{clean}{}     % clean standard theme with configured department style

%
% Configures colors and Logo for administration departments
%
\DeclareOption{dark}{}      % dark background and department colors

%
% Configures colors and Logo for administration departments
%
\DeclareOption{silver}{}    % silver background and department colors

%
% Configures colors and Logo for anything else
%
\DeclareOption*{\relax}           



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   IMPORTING PACKAGES
%
\ProcessOptions
\relax

% input encryption
\RequirePackage[utf8]{inputenc}

% font encryption
\RequirePackage[T1]{fontenc}

% language package
\RequirePackage[ngerman]{babel}

% Required for logo in header
\RequirePackage{tikz}

% Image and Graphic support
\RequirePackage{graphicx}

% Using 'libertine' as default font
% There is no LaTeX support for WINGS or HSW meta font
\RequirePackage{libertine}

% Package for Lorem ipsum ...
\RequirePackage{lipsum}

% Page geometry
\RequirePackage{geometry}

%
% Package provides defining colors
% e.g. \definecolor{mycolor}{RGB}{#R,#G,#B}
%      \color{mycolor}Text colored with mycolor
%
\RequirePackage{xcolor}

%
% Code-Snippets in Text
% e.g.: \lstinputlisting[language={...},caption={...},label={src:...}]{...}
% or:
%       \begin{lstlisting}
%       <code here>
%       \end{lstlisting}
%
% More @https://en.wikibooks.org/wiki/LaTeX/Source_Code_Listings
%
\RequirePackage{listings}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   EDITING BEAMER STYLE
%
\mode<presentation>
\usetheme{default}
\usecolortheme[RGB={\INSTITUTECOLORCODE}]{structure}
\useoutertheme{infolines}
\beamertemplatenavigationsymbolsempty
\addtobeamertemplate{frametitle}{}{%
    \begin{tikzpicture}[remember picture,overlay]
        \node[anchor=north east,yshift=-15pt,xshift=-15pt] at (current page.north east) {\includegraphics[scale=0.15]{\INSTITUTELOGO}};
    \end{tikzpicture}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   CONFIGURING PACKAGES
%

%
% Setting page margins
%
\geometry{left=1cm, right=1cm}

%
% Configuring Colors
%
% Color values taken from HSW Corporate Design Document:
% https://www.hs-wismar.de/hochschule/einrichtungen/oeffentlichkeitsarbeit/corporate-design/
%
\definecolor{black}{RGB}{0,0,0}
\definecolor{white}{RGB}{255,255,255}
\definecolor{wings}{RGB}{0,50,95}
\definecolor{lwings}{RGB}{100,150,195}
\definecolor{fiw}{RGB}{0,177,219}
\definecolor{fww}{RGB}{51,153,51}
\definecolor{fg}{RGB}{255,93,2}
\definecolor{dark}{RGB}{47,50,41}
\definecolor{silver}{RGB}{175,175,175}
\definecolor{lsilver}{RGB}{240,240,240}

%
% Configuring Listings and Sourcecodes
%
\lstset{
	backgroundcolor={\color{lsilver}},
	breakatwhitespace=false,
	showspaces=false,
	showstringspaces=false,
	basicstyle={\footnotesize\ttfamily\color{wings}},
	commentstyle={\footnotesize\ttfamily\color{dark}},
	keywordstyle={\footnotesize\ttfamily\color{fiw}},
	numberstyle={\footnotesize\ttfamily\color{black}},
	stringstyle={\footnotesize\ttfamily\color{fww}},
%	deletekeywords={},
%	morekeywords={},
%	title={Src: \lstname},
	breaklines={true},
	captionpos={b},
	frame={l},
	numbers=left,
	stepnumber=1,
	numbersep=10pt,
	tabsize=2,
	xleftmargin=2em
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   ADDITIONAL COMMANDS
%

% Insert Frame for new section
% @param1: Title for frame and section
% @param2: label for structure (Note: prefix for label is set as 'sec:')
\newcommand{\SectionFrame}[2]{
	\section{#1}
	\label{sec:#2}
	\frame{
		\flushright\includegraphics[scale=0.15]{img/hsw-wide.jpg}\\{\LARGE #1}
	}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   .
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   (PRE-)CONFIGURING TITLEPAGE 
%
\institute[\INSTITUTESHORT]{\INSTITUTE}
\titlegraphic{\includegraphics[scale=0.25]{\INSTITUTELOGO}}
